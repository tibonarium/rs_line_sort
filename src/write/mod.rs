use super::{ErrorKind, LogLine, SortError, LF};
use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader, BufWriter, Write};
use std::path::PathBuf;
use std::result;

pub struct PlaceAtLine {
    index: usize,
}

fn flush_cache(
    cache: &mut HashMap<usize, Vec<u8>>,
    current_line: usize,
    writer: &mut dyn Write,
) -> usize {
    let mut line = current_line;
    loop {
        match cache.remove(&line) {
            Some(cached_content) => {
                line += 1;
                write_line(writer, &cached_content[..]);
            }
            None => return line,
        }
    }
}

pub fn write_line(
    writer: &mut dyn Write,
    line: &[u8],
) -> Result<(), SortError> {
    let new_line = [&line[..], &[b'\n']].concat();

    writer.write_all(&new_line[..]).map_err(|e| {
        SortError::new(ErrorKind::IO, &format!("Failed ro write {:?}: {}", line, e))
    })?;

    Ok(())
}

pub fn write_all_lines(
    input_file: &PathBuf,
    output_file: &PathBuf,
    replace: &HashMap<usize, PlaceAtLine>,
) -> Result<(), SortError> {
    let reader = File::open(input_file)
        .map_err(|e| {
            SortError::new(
                ErrorKind::IO,
                &format!("Failed to open file {:?}: {}", input_file, e),
            )
        })
        .map(|f| BufReader::new(f))?;

    let mut writer = File::create(output_file)
        .map_err(|e| {
            SortError::new(
                ErrorKind::IO,
                &format!("Failed to open file {:?}: {}", output_file, e),
            )
        })
        .map(|f| BufWriter::new(f))?;

    let mut cache: HashMap<usize, Vec<u8>> = HashMap::new();

    reader
        .split(b'\n')
        .enumerate()
        .fold(0, |current_line, (line_number, content)| match content {
            Err(e) => {
                println!(
                    "Failed to read line_number:{} from {:?}: {}",
                    line_number, input_file, e
                );
                current_line
            }
            Ok(content) => match replace.get(&line_number) {
                Some(PlaceAtLine { index }) => {
                    if *index == current_line {
                        write_line(&mut writer, &content);

                        flush_cache(&mut cache, current_line + 1, &mut writer)
                    } else {
                        cache.insert(*index, content);
                        current_line
                    }
                }
                None => current_line,
            },
        });

    if cache.len() > 0 {
        write_line(&mut writer, &String::from("# MISSING LINES").into_bytes());

        for content in cache.values() {
            write_line(&mut writer, content);
        }
    }

    Ok(())
}

fn replace_all_lines(
    input_file: &PathBuf,
    lines: &Vec<LogLine>,
) -> Result<(), SortError> {
    let mut cache: HashMap<usize, Vec<u8>> = HashMap::new();

    {
        let reader = File::open(input_file)
            .map_err(|e| {
                SortError::new(
                    ErrorKind::IO,
                    &format!("Failed to open file {:?}: {}", input_file, e),
                )
            })
            .map(|f| BufReader::new(f))?;

        reader.split(LF).enumerate().fold(
            Ok(()),
            |result: result::Result<(), SortError>, (line_number, content)| match result {
                Err(_) => result,
                Ok(_) => match content {
                    Err(e) => Err(SortError::new(
                        ErrorKind::IO,
                        &format!(
                            "Failed to read line_number {} from {:?}: {}",
                            line_number, input_file, e
                        ),
                    )),
                    Ok(content) => {
                        cache.insert(line_number, content);
                        Ok(())
                    }
                },
            },
        )?;
    }

    {
        let mut writer = File::create(input_file)
            .map_err(|e| {
                SortError::new(
                    ErrorKind::IO,
                    &format!("Failed to open file {:?}: {}", input_file, e),
                )
            })
            .map(|f| BufWriter::new(f))?;

        println!("Writing to {:?} cache_size: {}", input_file, cache.len());

        lines.iter().fold(
            Ok(()),
            |result: result::Result<(), SortError>,
             LogLine {
                 line_number,
                 date,
                 content,
             }| match result {
                Err(_) => result,
                Ok(_) => match cache.get(&line_number) {
                    Some(content) => {
                        match std::str::from_utf8(&content[..]) {
                            Ok(text) => {
                                println!("line:{}: date:{:?} {}", line_number, date, text);
                            }
                            _ => {}
                        }

                        write_line(&mut writer, &content[..]);
                        Ok(())
                    }
                    None => Err(SortError::new(
                        ErrorKind::Method,
                        &format!(
                            "Can't find line_number: {} inside file {:?}",
                            line_number, input_file
                        ),
                    )),
                },
            },
        )?;
    }

    Ok(())
}
