use super::{ErrorKind, SortError};
use serde::Deserialize;
use std::fs;
use toml;

#[derive(Clone, Deserialize)]
pub struct Config {
    pub large_file: String,
    pub temp_dir: String,
    pub chunk_size: usize,
}

impl Config {
    pub fn from_file(filename: &str) -> Result<Self, SortError> {
        let content: String = fs::read_to_string(filename).map_err(|e| {
            SortError::new(
                ErrorKind::IO,
                &format!("Failed to read from {}: {}", filename, e),
            )
        })?;

        let result: Result<Config, toml::de::Error> = toml::from_str(&content);

        match result {
            Ok(r) => Ok(r),
            Err(e) => Err(SortError::new(
                ErrorKind::TOML,
                &format!("Failed to read TOML {}", e),
            )),
        }
    }
}
