use std::collections::HashMap;
use std::collections::VecDeque;
use std::fs::{self, File};
use std::io::{BufRead, BufReader, BufWriter, Write};
use std::path::PathBuf;
use std::result;

use chrono::NaiveDateTime;
use clap::{App, Arg};
use rand::prelude::*;

mod common;
mod config;
mod write;

use config::Config;

#[macro_use]
extern crate slog;
use slog::{Fuse, Logger};

const LF: u8 = b'\n';
const DATE_FORMAT: &str = "%d.%m.%Y %H:%M:%S%.3f";
const APPROVED_LETTERS: &str = "abcdefghijklmnopqrstuvwxyz123456789";
const NAME_LENGTH: usize = 10;

#[derive(Clone, Debug)]
enum ErrorKind {
    IO,
    TOML,
    Argument,
    Method,
}

#[derive(Clone, Debug)]
pub struct SortError {
    kind: ErrorKind,
    message: String,
}

impl SortError {
    fn new(
        kind: ErrorKind,
        message: &str,
    ) -> SortError {
        SortError {
            kind: kind,
            message: message.to_owned(),
        }
    }
}

#[derive(Clone, Debug)]
struct LogLine {
    line_number: usize,
    date: Option<i64>,
    content: Vec<u8>,
}

impl LogLine {
    fn new(line_number: usize, date: Option<i64>, content: &[u8]) -> Self {        
        LogLine {
            line_number: line_number,
            date: date,
            content: Vec::from(&content[..]),
        }
    }

    fn copy(&self) -> Self {
        LogLine {
            line_number: self.line_number,
            date: self.date,
            content: self.content[..].to_vec(),
        }
    }
}

fn main() -> Result<(), SortError> {
    let matches = App::new("Sort lines by date")
        .version("1.0")
        .name("sort_lines")
        .arg(
            Arg::with_name("config")
                .short("c")
                .long("config")
                .value_name("FILE")
                .help("Sets a custom config file")
                .default_value("sort_lines.toml")
                .takes_value(true),
        )
        .get_matches();

    let config_filename = match matches.value_of("config") {
        Some(c) => Ok(c),
        None => Err(SortError::new(ErrorKind::Argument, "No 'config' provided")),
    }?;

    let config = Config::from_file(config_filename)?;

    let large_file = PathBuf::from(config.large_file);
    let temp_dir = PathBuf::from(config.temp_dir);
    let chunk_size = config.chunk_size;

    let log = Logger::root(Fuse(common::PrintlnDrain), o!());

    sort_large_file(&log, &large_file, &temp_dir, chunk_size)?;

    Ok(())
}

pub fn date_to_string(date: i64) -> String {
    let sec = date / 1000;
    let date_time = NaiveDateTime::from_timestamp(sec, 0);
    date_time.format("%d.%m.%Y %H:%M:%S%").to_string()
}

pub fn parse_date(input: &str) -> Option<i64> {
    match NaiveDateTime::parse_from_str(input, DATE_FORMAT) {
        Ok(date) => {
            let timestamp = date.timestamp();
            let millis = date.timestamp_subsec_millis() as i64;
            Some(timestamp * 1000 + millis)
        }
        Err(e) => {
            println!("Failed to parse {}: {}", input, e);
            None
        }
    }
}

fn parse_line_content(
    line_number: usize,
    line: &Vec<u8>,
) -> LogLine {
    let v: Vec<&str> = match std::str::from_utf8(&line[..]) {
        Ok(content) => content.splitn(3, " ").collect(),
        _ => Vec::new(),
    };

    if v.len() >= 2 {
        let date_str = format!("{} {}", v[0], v[1]);

        LogLine::new(
            line_number,
            parse_date(&date_str),
            &line[..],
        )
    } else {
        LogLine::new(
            line_number,
            None,
            &line[..],
        )
    }
}

fn read_all_lines(input_file: &PathBuf) -> Result<Vec<LogLine>, SortError> {
    let reader = File::open(input_file)
        .map_err(|e| {
            SortError::new(
                ErrorKind::IO,
                &format!("Failed to open file {:?}: {}", input_file, e),
            )
        })
        .map(|f| BufReader::new(f))?;

    let result = reader
        .split(LF)
        .enumerate()
        .map(|(line_number, line)| match line {
            Err(e) => {
                println!(
                    "Failed to read line_number: {} from {:?}: {}",
                    line_number, input_file, e
                );

                LogLine::new(
                    line_number,
                    None,
                    &[],
                )
            }
            Ok(l) => parse_line_content(line_number, &l),
        })
        .collect::<Vec<LogLine>>();

    Ok(result)
}

fn less(
    a: &LogLine,
    b: &LogLine,
) -> bool {
    match a.date {
        None => true,
        Some(adate) => match b.date {
            None => false,
            Some(bdate) => adate < bdate,
        },
    }
}

fn sort_lines(lines: &mut Vec<LogLine>) {
    if lines.len() > 0 {
        let end = lines.len() - 1;
        merge_sort(&mut lines[..], 0, end);
    }
}

fn merge_sort(
    lines: &mut [LogLine],
    lo: usize,
    hi: usize,
) {
    if hi <= lo {
    } else {
        let mid = lo + (hi - lo) / 2;

        merge_sort(lines, lo, mid);
        merge_sort(lines, mid + 1, hi);
        merge_merge(lines, lo, mid, hi);
    }
}

fn merge_merge(
    vec: &mut [LogLine],
    lo: usize,
    mid: usize,
    hi: usize,
) {
    let mut i = lo;
    let mut j = mid + 1;
    let mut k = lo;

    let aux = (&vec[..]).to_vec();

    while k <= hi {
        // println!("lo:{} mid:{} hi:{} i:{} j:{} k:{} aux_len:{}", lo, mid, hi, i, j, k, aux.len());

        if i > mid {
            vec[k] = aux[j].copy();
            j += 1;
        } else if j > hi {
            vec[k] = aux[i].copy();
            i += 1;
        } else if less(&aux[j], &aux[i]) {
            vec[k] = aux[j].copy();
            j += 1;
        } else {
            vec[k] = aux[i].copy();
            i += 1;
        }
        k += 1;
    }
}

fn update_all_lines(
    input_file: &PathBuf,
    lines: &Vec<LogLine>,
) -> Result<(), SortError> {
    let mut writer = File::create(input_file)
        .map_err(|e| {
            SortError::new(
                ErrorKind::IO,
                &format!("Failed to open file {:?}: {}", input_file, e),
            )
        })
        .map(|f| BufWriter::new(f))?;

    println!("Updating lines at {:?}", input_file);

    lines.iter().fold(
        Ok(()),
        |result: result::Result<(), SortError>,
         LogLine {
             line_number: _,
             date: _,
             content,
         }| match result {
            Err(_) => result,
            Ok(_) => write::write_line(&mut writer, &content[..]),
        },
    )?;

    Ok(())
}

struct ManifestEntry {
    name: String,
    path: PathBuf,
}

impl ManifestEntry {
    fn new(
        name: String,
        path: PathBuf,
    ) -> Self {
        ManifestEntry {
            name: name,
            path: path,
        }
    }
}

struct Manifest<'a> {
    _size: usize,
    _lines: Vec<Vec<u8>>,
    _chunk_size: usize,
    _entries: HashMap<String, ManifestEntry>,
    saved_path: Option<PathBuf>,
    log: &'a Logger,
}

impl<'a> Manifest<'a> {
    fn new(log: &'a Logger) -> Self {
        Manifest {
            _size: 0,
            _chunk_size: 0,
            _lines: Vec::new(),
            _entries: HashMap::new(),
            saved_path: None,
            log: log,
        }
    }

    fn save_manifest(
        &mut self,
        temp_dir: &PathBuf,
    ) -> Result<(), SortError> {
        self.saved_path = Some({
            let mut tmp = PathBuf::from(temp_dir);
            tmp.push("manifest");
            tmp
        });

        if let Some(ref new_file) = self.saved_path {
            let mut writer = File::create(new_file)
                .map_err(|e| {
                    let msg = format!("Failed to create manifest {:?}: {}", new_file, e);
                    slog::error!(self.log, "{}", msg);

                    SortError::new(ErrorKind::IO, &msg)
                })
                .map(|f| BufWriter::new(f))?;

            self.entries().fold(
                Ok(()),
                |result: result::Result<(), SortError>, entry| match result {
                    Err(_) => result,
                    Ok(_) => {
                        let entry_path = entry.path.to_string_lossy().into_owned();
                        let line = format!("{} {}\n", entry.name, entry_path);

                        writer.write(line.as_bytes()).map(|s| ()).map_err(|e| {
                            let msg = format!("Failed to save line `{}`: {}", line, e);
                            slog::error!(self.log, "{}", msg);

                            SortError::new(ErrorKind::IO, &msg)
                        })
                    }
                },
            )?;
        }

        Ok(())
    }

    fn remove_manifest(&self) -> Result<(), SortError> {
        self.entries().fold(
            Ok(()),
            |result: result::Result<(), SortError>, next| match result {
                Err(_) => result,
                Ok(_) => fs::remove_file(&next.path).map_err(|e| {
                    let msg = format!("Failed to remove manifest entry {:?}: {}", next.path, e);
                    slog::error!(self.log, "{}", msg);

                    SortError::new(ErrorKind::IO, &msg)
                }),
            },
        )?;

        if let Some(ref saved_path) = self.saved_path {
            fs::remove_file(saved_path).map_err(|e| {
                let msg = format!("Failed to remove manifest {:?}: {}", saved_path, e);
                slog::error!(self.log, "{}", msg);

                SortError::new(ErrorKind::IO, &msg)
            })?;
        };

        Ok(())
    }

    pub fn store(
        &mut self,
        content: Vec<u8>,
    ) {
        self._chunk_size += content.len();
        if self._size < self._lines.len() {
            self._lines[self._size] = content;
        } else {
            self._lines.push(content);
        }
        self._size += 1;
    }

    pub fn current_lines(&self) -> impl Iterator<Item = &Vec<u8>> + '_ {
        self._lines[0..self._size].iter()
    }

    pub fn consume_lines(&mut self) -> Vec<Vec<u8>> {
        let mut lines = std::mem::replace(&mut self._lines, Vec::new());
        lines.truncate(self._size);

        self._size = 0;
        self._chunk_size = 0;

        lines
    }

    pub fn chunk_size(&self) -> usize {
        self._chunk_size
    }

    fn random_name(&self) -> String {
        let mut rng = rand::thread_rng();
        let mut bytes = APPROVED_LETTERS.to_string().into_bytes();

        loop {
            bytes.shuffle(&mut rng);
            let result = std::str::from_utf8(&bytes[..NAME_LENGTH]);

            match result {
                Err(_) => {}
                Ok(name) => match self._entries.get(name) {
                    None => return String::from(name),
                    Some(_) => {}
                },
            }
        }
    }

    fn add_entry(
        &mut self,
        name: String,
        entry: ManifestEntry,
    ) {
        self._entries.insert(name, entry);
    }

    fn entries(&self) -> impl Iterator<Item = &ManifestEntry> {
        self._entries.values()
    }

    fn save_chunk(
        &mut self,
        temp_dir: &PathBuf,
    ) -> result::Result<(), SortError> {
        let name = self.random_name();

        let mut file_path = PathBuf::from(temp_dir);
        file_path.push(&name);

        let mut result = self
            .current_lines()
            .enumerate()
            .map(|(line_number, line)| parse_line_content(line_number, &line))
            .collect::<Vec<LogLine>>();

        sort_lines(&mut result);

        let mut writer = File::create(&file_path).map_err(|e| {
            let msg = format!("Failed to create file chunk {:?}: {}", file_path, e);
            // slog::error!(self.log, "{}", msg);

            SortError::new(ErrorKind::IO, &msg)
        })?;

        let data = self
            .consume_lines()
            .iter()
            .map(|v| [&v[..], &[LF]].concat())
            .flatten()
            .collect::<Vec<_>>();

        let _res = match writer.write_all(&data[..]) {
            Err(e) => Err(SortError::new(
                ErrorKind::IO,
                &format!("Failed to save chunk info {:?}: {}", file_path, e),
            )),
            Ok(_) => Ok(()),
        }?;

        let entry_name = String::from(&name);
        self.add_entry(entry_name, ManifestEntry::new(name, file_path));

        Ok(())
    }
}

struct ManifestEntryDeque {
    queue: VecDeque<ManifestEntry>,
    lines: Vec<LogLine>,
}

impl ManifestEntryDeque {
    fn new() -> Self {
        ManifestEntryDeque {
            queue: VecDeque::new(),
            lines: Vec::new(),
        }
    }

    pub fn pop_front(&mut self) -> Option<ManifestEntry> {
        self.queue.pop_front()
    }

    pub fn push_back(
        &mut self,
        value: ManifestEntry,
    ) {
        self.queue.push_back(value)
    }

    pub fn queue_len(&self) -> usize {
        self.queue.len()
    }

    pub fn append_lines(
        &mut self,
        other: &mut Vec<LogLine>,
    ) {
        self.lines.append(other);
    }

    pub fn lines_len(&self) -> usize {
        self.lines.len()
    }

    pub fn sort_lines(&mut self) {
        sort_lines(&mut self.lines);
    }

    fn drain_and_update_entry(
        &mut self,
        drain_size: usize,
        entry: &ManifestEntry,
    ) -> Result<ManifestEntry, SortError> {
        let head = self.lines.drain(..drain_size).collect::<Vec<_>>();

        update_all_lines(&entry.path, &head)?;

        let replace_entry =
            ManifestEntry::new(String::from(&entry.name), PathBuf::from(&entry.path));
        Ok(replace_entry)
    }
}

fn sort_large_file<'a>(
    log: &'a Logger,
    large_file: &PathBuf,
    temp_dir: &PathBuf,
    chunk_size: usize,
) -> result::Result<(), SortError> {
    let reader = File::open(large_file)
        .map_err(|e| {
            SortError::new(
                ErrorKind::IO,
                &format!("Failed to open large file {:?}: {}", large_file, e),
            )
        })
        .map(|f| BufReader::new(f))?;

    let mut manifest: Manifest = reader.split(LF).fold(
        Ok(Manifest::new(log)),
        |result: result::Result<Manifest, SortError>, content| match result {
            Err(_) => result,
            Ok(mut manifest) => match content {
                Err(e) => Err(SortError::new(
                    ErrorKind::IO,
                    &format!(
                        "Failed to read line from large file {:?}: {}",
                        large_file, e
                    ),
                )),
                Ok(content) => {
                    manifest.store(content);

                    if manifest.chunk_size() > chunk_size {
                        manifest.save_chunk(temp_dir)?;
                    }

                    Ok(manifest)
                }
            },
        },
    )?;

    if manifest.chunk_size() > 0 {
        manifest.save_chunk(temp_dir)?;
    }

    manifest.save_manifest(temp_dir)?;

    let total_entries = manifest.entries().count();

    let entry_deque = manifest.entries().fold(
        Ok(ManifestEntryDeque::new()),
        |result: result::Result<ManifestEntryDeque, SortError>, next| match result {
            Err(_) => result,
            Ok(mut deque) => match read_all_lines(&next.path) {
                Err(e) => {
                    return Err(SortError::new(
                        ErrorKind::IO,
                        &format!("Failed to read all lines from {:?}: {:?}", next.path, e),
                    ))
                }
                Ok(mut next_lines) => {
                    deque.append_lines(&mut next_lines);
                    let mut original_queue_size = deque.queue_len();

                    println!(
                        "queue: {} total: {} lines_remaining: {}",
                        original_queue_size,
                        total_entries,
                        deque.lines_len()
                    );

                    while original_queue_size > 0 {
                        if let Some(next_entry) = deque.pop_front() {
                            original_queue_size -= 1;

                            match read_all_lines(&next_entry.path) {
                                Err(e) => {
                                    return Err(SortError::new(
                                        ErrorKind::IO,
                                        &format!(
                                            "Failed to read all lines from {:?}: {:?}",
                                            next_entry.path, e
                                        ),
                                    ))
                                }
                                Ok(mut lines) => {
                                    let original_size = lines.len();
                                    deque.append_lines(&mut lines);
                                    deque.sort_lines();

                                    match deque.drain_and_update_entry(original_size, &next_entry) {
                                        Err(e) => return Err(e),
                                        Ok(replace_entry) => {
                                            deque.push_back(replace_entry);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    deque.sort_lines();
                    let remaining_size = deque.lines_len();

                    match deque.drain_and_update_entry(remaining_size, &next) {
                        Err(e) => Err(e),
                        Ok(replace_entry) => {
                            deque.push_back(replace_entry);

                            Ok(deque)
                        }
                    }
                }
            },
        },
    )?;

    slog::info!(log, "merging small files");
    merge_small_files(large_file, &entry_deque)?;

    slog::info!(log, "removing temporary files & manifest");
    manifest.remove_manifest()?;

    Ok(())
}

fn merge_small_files(
    large_file: &PathBuf,
    manifest: &ManifestEntryDeque,
) -> Result<(), SortError> {
    let ManifestEntryDeque { queue, lines: _ } = manifest;

    let mut large_writer = File::create(large_file)
        .map_err(|e| {
            SortError::new(
                ErrorKind::IO,
                &format!("Failed to open large file {:?}: {}", large_file, e),
            )
        })
        .map(|f| BufWriter::new(f))?;

    let result = queue.iter().fold(
        Ok(()),
        |result: result::Result<(), SortError>, next_entry| match result {
            Err(_) => result,
            Ok(_) => {
                let reader = File::open(&next_entry.path).map(|f| BufReader::new(f));

                match reader {
                    Err(e) => Err(SortError::new(
                        ErrorKind::IO,
                        &format!("Failed to open file {:?}: {}", next_entry.path, e),
                    )),
                    Ok(reader) => reader.split(LF).fold(
                        Ok(()),
                        |result: result::Result<(), SortError>, line| match result {
                            Err(_) => result,
                            Ok(_) => {
                                let line = line.map_err(|e| {
                                    SortError::new(
                                        ErrorKind::IO,
                                        &format!(
                                            "Failed to read line from {:?}: {}",
                                            next_entry.path, e
                                        ),
                                    )
                                })?;

                                write::write_line(&mut large_writer, &line[..])
                            }
                        },
                    ),
                }
            }
        },
    )?;

    Ok(result)
}

#[cfg(test)]
mod tests;
