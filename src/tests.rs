use super::*;

#[test]
fn test_date_conversion() {
    let result = parse_date("27.12.2019 16:01:39.762");
    assert_eq!(result.is_some(), true);
}

#[test]
fn test_slice_length() {
    let v = vec![1, 2, 3];
    let lo = 0;
    let hi = 1;
    let s = &v[lo..hi + 1];
    assert_eq!(s.len(), 2);
}
