# Description

This application can be used to demonstrate practical application of sorting algorithms

# How to configure

Application will try to find `sort_lines.toml` inside directory from which it was called. Or path to configuration file can be provided using command line arguments
```
rs_line_sort --config "path\to\sort_lines.toml"
```

Example of how configuration file can look
```
large_file = "path\\to\\large\\file"
temp_dir = "path\\to\\temporary\\directory"
chunk_size = 1024
```

* large_file - Path to the large file whose lines we need to sort
* temp_dir - Temporary directory where small chuncks will be created and sorted
* chunck_size - approximate size of each chunk

If we are using windows then we need to escape path `D:\\FILES\\CODE`

# Assumptions about Large file structure

We are assuming that large file has a following structure

```
27.12.2019 15:00:39.762 <content_of_line_1>
27.12.2019 15:01:39.762 <content_of_line_2>
```

To put it simply

* Each line has a date
* Date has a following format `%d.%m.%Y %H:%M:%S%.3f`
